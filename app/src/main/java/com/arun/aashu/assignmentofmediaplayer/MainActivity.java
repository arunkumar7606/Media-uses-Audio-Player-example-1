package com.arun.aashu.assignmentofmediaplayer;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {


    ListView lv;
    Button b1;
    MediaPlayer mp;
    private String lv_arr[]={"test 1","test 2","test 3","test 4","test 5"};

    ArrayList<String> arrayList=new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = findViewById(R.id.listview1);
        b1 = findViewById(R.id.button1);


        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lv_arr);

        lv.setAdapter(adapter);

        final MediaPlayer mp1 = MediaPlayer.create(getApplicationContext(), R.raw.jadu);
        final MediaPlayer mp2 = MediaPlayer.create(getApplicationContext(), R.raw.jadu1);
        final MediaPlayer mp3 = MediaPlayer.create(getApplicationContext(), R.raw.jadu2);
        final MediaPlayer mp4 = MediaPlayer.create(getApplicationContext(), R.raw.jadu3);
        final MediaPlayer mp5 = MediaPlayer.create(getApplicationContext(), R.raw.jadu4);



        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (lv.getItemAtPosition(position) == "test 1") {
                    mp1.start();
                    mp1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();



                        }
                    });
                }

                if (lv.getItemAtPosition(position) == "test 2") {
                    mp1.pause();
                    mp2.start();
                    mp2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                        }
                    });
                }

                if (lv.getItemAtPosition(position) == "test 3") {
                    mp2.pause();
                    mp3.start();
                    mp3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                        }
                    });
                }

                if (lv.getItemAtPosition(position) == "test 4") {
                    mp3.pause();
                    mp4.start();
                    mp4.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();


                        }
                    });
                }

                if (lv.getItemAtPosition(position) == "test 5") {
                    mp4.pause();
                    mp5.start();
                    mp5.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                        }
                    });
                }



            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp1.stop();
                mp2.stop();
                mp3.stop();
                mp4.stop();
                mp5.stop();
            }
        });

    }
}